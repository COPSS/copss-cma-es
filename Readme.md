**COPSS-CMA-ES 0.3.0**
=========================

**COPSS-CMA-ES** solves for charges on dielectric particles from a given set of particle trajectories. It is based on the Covariance Matrix Adaption Evolutionary Strategy (CMA-ES) and the force field for electrostatic interactions between polarizable dielectric particles. CMA-ES is used to minimize the difference between simulated trajectory and the target trajectory, and inversely calcualte charges on individual particles. The simulated trajectory is calculated by [**LAMMPS**](http://lammps.sandia.gov/) coupled with the polarizable force field at every CMA-ES generation. COPSS is coupled with LAMMPS and an external CMA-ES library [**libcmaes**](https://github.com/beniz/libcmaes) to solve this inverse problem.


**Installation**
-------------------------------------------
COPSS-CMA-ES is written using [**libcmaes**](https://github.com/beniz/libcmaes) and [**LAMMPS**](http://lammps.sandia.gov/) as libraries. Before compiling COPSS-CMA-ES, you need to install LAMMPS and libcmaes.
###0. System environment prep
You should have the following software loaded or compiled

 - [CMAKE](https://cmake.org/) (version 2.8.12 or later)
 - [GCC](https://gcc.gnu.org/) (version 4.8.4 or later)
 - [PYTHON](https://www.python.org/) (python 2)
 - [OPENMPI](https://www.open-mpi.org/) (version 1.10.4 or later) or [MVAPICH](http://mvapich.cse.ohio-state.edu/) (version mvapich2 2.2b or later)



###1. Install libcmaes

 - Install libcmaes by following instructions in 'Build' section at [its website](https://github.com/beniz/libcmaes)


###2. Install LAMMPS as a library

 - Go to https://github.com/jinselove/colloid-salt-polarization-image-method, and download `fix_colloidImage.cpp` and `fix_colloidImage.h`

 - Download LAMMPS, and put `fix_colloidImage.cpp` and `fix_colloidImage.h` in folder `your_path_to_lammps/src`

 - Compile LAMMPS as a static library by typing `make mode=lib mpi`, and as a shared library by typing `make mode=shlib mpi`


###3. Edit your bashrc file

 - Add dynamic linking path in your `.bashrc` file by adding the line below, for example,

   `export LD_LIBRARY_PATH=$MPI_HOME/lib:$MPI_HOME/lib/openmpi:/home/xikai/Softwares/libcmaes/build/lib:/home/xikai/Softwares/lammps-16Feb16/src:$LD_LIBRARY_PATH`


###3. Compile COPSS-CMA-ES

 - Compile the code in `CMA-ES/src` folder in this repository using the command below

   `mpic++ -fopenmp -std=gnu++11 -I/usr/include/eigen3/ -I/home/xikai/Softwares/libcmaes/build/include/libcmaes/ -I/home/xikai/Softwares/lammps-16Feb16/src -L/home/xikai/Softwares/libcmaes/build/lib/ -L/home/xikai/Softwares/lammps-16Feb16/src/ -o run_inverse inverse_design.C -lcmaes -llammps_mpi`


###4. Run the program

 - Run the optimization code by, for example, typing the followings in `CMA-ES/cmaes-10particles` folder

   `../src/run_inverse`



**Contact**
-------------------------------------------
 
 -  If you need help, have any questions or comments, join our mailing list [copss-users@googlegroups.com](Link URL)

     **GMail users**: just click "Join group" button

     **Everyone else**: send an email to [copss-users+subscribe@googlegroups.com](Link URL)

**Algorithm**
-------------------------------------------
 Our method paper has details on the tests of this code: [Evolutionary strategy for inverse charge measurements of dielectric particles](https://arxiv.org/abs/1805.08371)


**Contributors**
-------------------------------------------
 
 - [**Xikai Jiang**](https://www.researchgate.net/profile/Xikai_Jiang), Institute for Molecular Engineering, The University of Chicago. [EMAIL](xikai@uchicago.edu)

 - [**Jiyuan Li**](https://scholar.google.com/citations?user=XE6JtJwAAAAJ&hl=en), Institute for Molecular Engineering, The University of Chicago. [LinkedIn](https://www.linkedin.com/in/jyliuchicago/) , [EMAIL](jyli@uchicago.edu)
 
  

**License**
-------------------------------------------
 
* The codes are open-source and distributed under the GNU GPL license, and may not be used for any commercial or for-profit purposes without our permission.



**Release history of COPSS**
-------------------------------------------
 
 - 0.3.0 (Release COPSS-CMA-ES module)

 - 0.2.0 (Release COPSS-Hydrodynamics module)

 - 0.1.0 (Release COPSS-Polarization module)

