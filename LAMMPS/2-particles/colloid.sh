#!/bin/bash

#SBATCH --job-name=colloids.sh
#SBATCH --output=colloids.out
#SBATCH --error=colloids.err
#SBATCH --partition=westmere
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jyli@uchicago.edu
./lmp_mpi < in.test
