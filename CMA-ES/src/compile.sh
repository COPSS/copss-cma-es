#!/bin/bash

set -x

# on local desktop
mpic++ -fopenmp -std=gnu++11 -I/usr/include/eigen3 -I/home/xikai/Softwares/libcmaes/build/include/libcmaes/ -I/home/xikai/Softwares/lammps-30Jul16/src -L/home/xikai/Softwares/libcmaes/build/lib/ -L/home/xikai/Softwares/lammps-30Jul16/src/ -o run_inverse inverse_design.C -lcmaes -llammps_mpi

# on Blues
#mpic++ -fopenmp -std=gnu++11 -I/soft/spack-0.9.1/opt/spack/linux-centos6-x86_64/gcc-6.1.0/eigen-3.2.10-d7bgbiri4tnysqgqvgsw5a6jsqbegehj/include/eigen3 -I/home/xikai/Softwares/libcmaes/build/include/libcmaes/ -I/home/xikai/Softwares/lammps-16Feb16/src -L/home/xikai/Softwares/libcmaes/build/lib/ -L/home/xikai/Softwares/lammps-16Feb16/src/ -o run_inverse inverse_design.C -lcmaes -llammps_mpi


