/**
 * Global variables
 */

// How many particles
const unsigned int NParticles = 3;

// How many frames in experiment
const unsigned int NFrames = 25;

// Coordinates from experiment
//const double *exp_c = new double[3*Nparticles*Nframes];
double exp_c[NFrames][3*NParticles];
