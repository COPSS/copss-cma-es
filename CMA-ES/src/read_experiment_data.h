/**
 * Function to read in experimental trajectories
 */

/**
 * Sample experiment data format for 2 particles
 * 1ms
 * x y z
 * x y z
 * 2ms
 * x y z
 * x y z
 * 3ms
 * x y z
 * x y z
 * ...
 */

void read_experiment_data()
{
	// Open a file stream
	std::ifstream f_trj_exp("experiment.dat");

	// If file opens correctly, do nothing
	if (f_trj_exp.is_open()) {}
	// If not open correctly, print error message and terminate program
	else { std::cout << "Error opening file: experiment.dat" <<std::endl; _exit(1); }

	// Temporary varialbes to read data from file
	std::string marker;
	double exp_x, exp_y, exp_z;

	// Loop through frames
        for(int i=0;i<NFrames;i++)
	{
		// Read markers
		f_trj_exp >> marker;

		for (int j=0;j<NParticles;j++)
		{
			// Experimental trajectory
			f_trj_exp >> exp_x >> exp_y >> exp_z;

			// Deviation between simulation and experiment
			exp_c[i][3*j   ] = exp_x;
			exp_c[i][3*j +1] = exp_y;
			exp_c[i][3*j +2] = exp_z;
		}
	}

	// Clean up
	f_trj_exp.close();
}
