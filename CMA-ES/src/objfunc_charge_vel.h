/**
 * Objective function to minimize.
 * Here it's the error in trajectories between simulation and experiment.
 * Check line 47 & 55 before a simulation.
 */
FitFunc RMS = [](const double *x, const int N)
{
	// If MPI has not been initialized, then MPI_Init
	// If MPI has been initialized, then do not MPI_Init
	int initialized, provided;
	MPI_Initialized(&initialized);

	if (!initialized) {
//         std::cout << "Before mpi initialized" << std::endl;
           MPI_Init(NULL,NULL);
//	   std::cout << "After mpi initialized" << std::endl;
	}
//	if (!initialized) { MPI_Init_thread(NULL,NULL,MPI_THREAD_SINGLE,&provided); }

	// Instantiate LAMMPS
	int narg = 3;
	char **arg = new char*[narg];
	for (int i=0; i<narg; i++) arg[i] = new char[8];

	sprintf(arg[0],"lmp");
	sprintf(arg[1],"-sc");
	sprintf(arg[2],"none"); // no screen output

	LAMMPS *lmp = new LAMMPS(narg,arg,MPI_COMM_WORLD);

	// Setup simulation by LAMMPS script
	lmp->input->file("in.lammps");

	// Transfer Const to Non-const array because
	// lammps_scatter_atoms needs Non-const array
	double *charge_in = new double[NParticles];
	for (unsigned int i=0;i<NParticles;i++) { charge_in[i] = x[i] * 1e-1; }
	//charge_in[1] = x[1] * 1e-1;
	//charge_in[2] = x[2] * 1e-1;

	// Assign charges to lammps (refer src/library.cpp in Lammps source code)
	char lmp_charge[2]="q";
	lammps_scatter_atoms(lmp, lmp_charge, 1, 1, charge_in);
	delete [] charge_in;

	// Velocities
	unsigned int N3 = 3*NParticles;
	double *vel_in = new double[N3];
	for (unsigned int i=0;i<N3;i++) { vel_in[i] = x[NParticles + i]; }//For similar sensitivity
	vel_in[0] = x[3] * 1e-3;
	vel_in[1] = x[4] * 1e-2;
	vel_in[2] = x[5] * 1e-11;
        vel_in[3] = x[6] * 1e1;
	vel_in[5] = x[8] * 1e-12;
        vel_in[6] = x[9] * 1e1;
	vel_in[8] = x[11] * 1e-11; //velocity in z-direction are all zero

	// Assign velocities to lammps
	char lmp_vel[2]="v";
	lammps_scatter_atoms(lmp, lmp_vel, 1, 3, vel_in);
	delete [] vel_in;

	// Run nsteps for 1 frame
	int nsteps  = 1000;
	char str[32];
	sprintf(str,"run %d post no",nsteps);

	// Root mean square errors
	double rms = 0.0;

	// Loop through frames and run simulation
        for(int i=0;i<NFrames;i++)
	{
		// Run simulation for one frame
		lmp->input->one(str);

		// Get coordinates from simulation
		double *sim_x = new double[N3];
		char lmp_x[2]="x";
		lammps_gather_atoms(lmp,lmp_x,1,3,sim_x);

		double tmp = 0., diffx, diffy, diffz;

		// Loop through all particles
		for (int j=0;j<NParticles;j++)
		{
			// Deviation between simulation and experiment
			diffx = sim_x[3*j  ] - exp_c[i][3*j  ];
			diffy = sim_x[3*j+1] - exp_c[i][3*j+1];
			diffz = sim_x[3*j+2] - exp_c[i][3*j+2];
			tmp += (diffx*diffx + diffy*diffy + diffz*diffz);
		}

		rms += sqrt( tmp / double(NParticles) );
		delete [] sim_x;
	}

	// Clean up
	delete lmp;
	//MPI_Finalize();

	// Average rms over frames
        rms /= double(NFrames);

	// Print results
	std::cout << "x: "; // Print this guess to screen
	for (int i=0;i<N;i++) { std::cout << x[i] << " "; }
	std::cout << " " << rms << std::endl;

	return rms;
};
