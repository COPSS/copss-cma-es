#include <cmath>
#include <cstdio>
#include <mpi.h>
#include "lammps.h"
#include "input.h"
#include "library.h"

using namespace LAMMPS_NS;

#define NPARAMETERS 10
#define NOBSERVATIONS 3000

int main(int argc,char **argv)
{
  FILE *fp;
  LAMMPS *lmp;
  double *x, *y, *f, *e, rms;
  int i, t;
  char lmp_str[32];
  const char *lmp_arg[3] = {"lmp", "-sc", "none"};
  const char *lmp_charge = "q";
  const char *lmp_x = "x";
  const int lmp_frames = 100;
  const int lmp_steps = 1000;

  MPI_Init(&argc, &argv);

  if (argc < 3) {
    printf("Usage: %s <input> <output>\n", argv[0]);
    return 1;
  }

  x = new double[NPARAMETERS];
  y = new double[3*NPARAMETERS];
  f = new double[NOBSERVATIONS];
  e = new double[NOBSERVATIONS];

  fp = fopen(argv[1], "r");
  x[NOBSERVATIONS-1] = 0.0;
  if (!fp) {
    printf("Could not open %s\n", argv[1]);
    return 1;
  }

  for (i = 0; i < NPARAMETERS-1; ++i) {
    if (1 != fscanf(fp, "%lf", x+i)) {
      printf("File %s has wrong format\n", argv[1]);
      return 1;
    }
    x[NPARAMETERS-1] -= x[i];
  }
  fclose(fp);

  fp = fopen("experiment.dat", "r");
  if (!fp) {
    printf("Could not open experiment.dat\n");
    return 1;
  }
  for (t = 0; t < lmp_frames; ++t) {
    if (1 != fscanf(fp, "%d", &i)) {
      printf("File experiment.dat has wrong format\n");
      return 1;
    }
    for (i = 0; i < NPARAMETERS; ++i) {
      if (3 != fscanf(fp, "%lf%lf%lf", e+3*NPARAMETERS*t + 3*i + 0, e+3*NPARAMETERS*t + 3*i + 1, e+3*NPARAMETERS*t + 3*i + 2)) {
        printf("File experiment.dat has wrong format\n");
        return 1;
      }
    }
  }
  fclose(fp);

  lmp = new LAMMPS(3,(char**)lmp_arg,MPI_COMM_WORLD);

  /* Setup simulation by LAMMPS script */
  lmp->input->file("in.lammps");

  /* Assign charges to particles */
  lammps_scatter_atoms(lmp,(char*)lmp_charge,1,1,x);

  /* Run 1 milisecond if timestep is 1 microsecond */
  sprintf(lmp_str,"run %d post no", lmp_steps);

  for (t = 0; t < lmp_frames; ++t) {
    lmp->input->one(lmp_str);

    /* Get coordinates from simulation */
    lammps_gather_atoms(lmp,(char*)lmp_x,1,3,y);

    /* Compute residuals */
    for (i = 0; i < NPARAMETERS; ++i) {
      f[3*NPARAMETERS*t + 3*i + 0] = y[3*i+0] - e[3*NPARAMETERS*t + 3*i + 0];
      f[3*NPARAMETERS*t + 3*i + 1] = y[3*i+1] - e[3*NPARAMETERS*t + 3*i + 1];
      f[3*NPARAMETERS*t + 3*i + 2] = y[3*i+2] - e[3*NPARAMETERS*t + 3*i + 2];
    }
  }

  fp = fopen(argv[2], "w");
  rms = 0.0;
  for (i = 0; i < NOBSERVATIONS; ++i) {
    fprintf(fp, "%20.19e\n", f[i]);
    rms += f[i]*f[i];
  }
  rms = sqrt(rms);
  fclose(fp);

  printf("rms: %5.4e\n", rms);

  delete lmp;
  delete [] x;
  delete [] y;
  delete [] f;
  return 0;
}

