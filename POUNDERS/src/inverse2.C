/*
   Include "petsctao.h" so that we can use TAO solvers.  Note that this
   file automatically includes libraries such as:
     petsc.h       - base PETSc routines   petscvec.h - vectors
     petscsys.h    - sysem routines        petscmat.h - matrices
     petscis.h     - index sets            petscksp.h - Krylov subspace methods
     petscviewer.h - viewers               petscpc.h  - preconditioners

*/

#include <petsctao.h>
#include <iostream>
#include <fstream>

static char help[]="Finds the nonlinear least-squares solution";

#define NPARAMETERS 10
#define NOBSERVATIONS 31

/* User provided Routines */

/* ------------------------------------------------------------ */
#undef __FUNCT__
#define __FUNCT__ "FormStartingPoint"
static PetscErrorCode FormStartingPoint(Vec X)
{
  PetscReal      *x;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(X,&x);CHKERRQ(ierr);
  x[0] = +3.0 - 0.5;
  x[1] = -3.0 + 0.5;
  x[2] = -3.0 - 0.5;
  x[3] = -3.0 + 0.5;
  x[4] = +3.0 - 0.5;
  x[5] = -3.0 + 0.5;
  x[6] = +3.0 - 0.5;
  x[7] = +3.0 + 0.5;
  x[8] = +3.0 - 0.5;
  x[9] = -3.0 + 0.5;
  VecRestoreArray(X,&x);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode EvaluateFunction(Tao tao,Vec X,Vec F,void *ptr);

/*--------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  PetscErrorCode ierr;           /* used to check for functions returning nonzeros */
  Vec            x,xl,xu,f;      /* solution, function */
  Tao            tao;            /* Tao solver context */
  PetscReal      hist[100],resid[100];
  PetscInt       lits[100];

  std::cout << "init" << std::endl;
  ierr = PetscInitialize(&argc,&argv,0,help);CHKERRQ(ierr);
  std::cout << "start" << std::endl;

  /* Allocate vectors */
  ierr = VecCreateSeq(MPI_COMM_SELF,NPARAMETERS,&x);CHKERRQ(ierr);
  ierr = VecCreateSeq(MPI_COMM_SELF,NOBSERVATIONS,&f);CHKERRQ(ierr);

  /* Create TAO solver and set desired solution method */
  ierr = TaoCreate(PETSC_COMM_SELF,&tao);CHKERRQ(ierr);
  ierr = TaoSetType(tao,TAOPOUNDERS);CHKERRQ(ierr);

  /* Set the starting point. */
  ierr = FormStartingPoint(x);CHKERRQ(ierr);
  ierr = TaoSetInitialVector(tao,x);CHKERRQ(ierr);

  /* Set the bounds on the parameters */
  ierr = VecDuplicate(x,&xl);CHKERRQ(ierr);
  ierr = VecDuplicate(x,&xu);CHKERRQ(ierr);
  ierr = VecSet(xl,-10.0);CHKERRQ(ierr);
  ierr = VecSet(xu,+10.0);CHKERRQ(ierr);
  ierr = TaoSetVariableBounds(tao,xl,xu);CHKERRQ(ierr);

  /* Set the objective function */
  ierr = TaoSetSeparableObjectiveRoutine(tao,f,EvaluateFunction,PETSC_NULL);CHKERRQ(ierr);

  /* Check for any TAO command line arguments */
  ierr = TaoSetFromOptions(tao);CHKERRQ(ierr);
  ierr = TaoSetConvergenceHistory(tao,hist,resid,0,lits,100,PETSC_TRUE);CHKERRQ(ierr);

  /* Perform the Solve */
  ierr = TaoSolve(tao);CHKERRQ(ierr);

  /* Free TAO data structures */
  ierr = TaoDestroy(&tao);CHKERRQ(ierr);

   /* Free PETSc data structures */
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&xl);CHKERRQ(ierr);
  ierr = VecDestroy(&xu);CHKERRQ(ierr);
  ierr = VecDestroy(&f);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}

#undef MIN
#undef MAX
#include "lammps.h"
#include "input.h"
#include "library.h"

using namespace LAMMPS_NS;

/*--------------------------------------------------------------------*/
#undef __FUNCT__
#define __FUNCT__ "EvaluateFunction"
static PetscErrorCode EvaluateFunction(Tao tao,Vec X,Vec F,void *ptr)
{
  PetscReal      *x,*f;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = VecGetArray(X,&x);CHKERRQ(ierr);
  ierr = VecGetArray(F,&f);CHKERRQ(ierr);

  // Initialize lammps simulation
  int narg = 3;
  char **arg = new char*[narg];
  for (int i=0; i<narg; i++) arg[i] = new char[8];

  sprintf(arg[0],"lmp");
  sprintf(arg[1],"-sc");
  sprintf(arg[2],"none");

  LAMMPS *lmp = new LAMMPS(narg,arg,PETSC_COMM_WORLD);

  // Setup simulation by LAMMPS script
  lmp->input->file("in.lammps");

  // Initialize inputs and outputs
  double *x_in = new double[NPARAMETERS];
  for (int i=0; i<NPARAMETERS; i++) x_in[i] = x[i];
  for (int i=0; i<NOBSERVATIONS; i++) f[i] = 0;

  // Assign charges to particles
  char lmp_charge[2]="q";
  lammps_scatter_atoms(lmp,lmp_charge,1,1,x_in);

  // How many miliseconds to run
  int nframes = 100;

  // Run 1 milisecond if timestep is 1 microsecond
  int nsteps  = 1000;
  char str[32];
  sprintf(str,"run %d post no",nsteps);

  // Read in experimental trajectory
  std::ifstream f_trj_exp("experiment.dat");

  // If file opens correctly, do nothing
  if (!f_trj_exp.is_open()) {
    std::cout << "Error opening file: experiment.dat" << std::endl;
    exit(1); 
  }

  // Run simulation
  double *sim_x = new double[3*NPARAMETERS];
  double exp_x, exp_y, exp_z;
  double rms = 0;
  char lmp_x[2]="x";
  std::string marker;

  for (int t=0; t<nframes; t++) {
    lmp->input->one(str);

    // Get coordinates from simulation
    lammps_gather_atoms(lmp,lmp_x,1,3,sim_x);

    // Read markers
    f_trj_exp >> marker;

    for (int i=0; i<NPARAMETERS; i++) {
      // Experimental trajectory
      f_trj_exp >> exp_x >> exp_y >> exp_z;

      // Deviation between simulation and experiment
      f[3*i + 0] += (sim_x[3*i+0] - exp_x)*(sim_x[3*i+0] - exp_x);
      f[3*i + 1] += (sim_x[3*i+1] - exp_y)*(sim_x[3*i+1] - exp_y);
      f[3*i + 2] += (sim_x[3*i+2] - exp_z)*(sim_x[3*i+2] - exp_z);

      rms += (sim_x[3*i+0] - exp_x)*(sim_x[3*i+0] - exp_x) + (sim_x[3*i+1] - exp_y)*(sim_x[3*i+1] - exp_y) + (sim_x[3*i+2] - exp_z)*(sim_x[3*i+2] - exp_z);
    }
  }
  for (int i=0; i<NPARAMETERS; i++) {
    f[3*i + 0] = sqrt(f[3*i + 0]);
    f[3*i + 1] = sqrt(f[3*i + 1]);
    f[3*i + 2] = sqrt(f[3*i + 2]);
  }
  f[30] = 10*(x_in[0] + x_in[1] + x_in[2] + x_in[3] + x_in[4] + x_in[5] + x_in[6] + x_in[7] + x_in[8] + x_in[9]);
  delete [] sim_x;

  // Cleanup
  f_trj_exp.close();
  delete [] x_in;
  delete lmp;

  std::cout << "x: "; // Print this guess to screen
  for (int i=0;i<NPARAMETERS;i++) { std::cout << x[i] << " "; }
  std::cout << "rms: " << sqrt(rms) << std::endl;

  ierr = VecRestoreArray(X,&x);CHKERRQ(ierr);
  ierr = VecRestoreArray(F,&f);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

